package gof.factory;

public class Pentagon implements Polygon {

    @Override
    public void getType() {
        System.out.println("This is pentagon type");
    }
    
}
