package gof.factory;

public class Client {
    public static void main(String[] args) {
        PolygonFactory pg = new PolygonFactory();
        pg.getPolygon(3).getType();
        pg.getPolygon(4).getType();
        pg.getPolygon(5).getType();
    }
}
