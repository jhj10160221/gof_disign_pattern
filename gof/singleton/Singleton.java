package gof.singleton;

public class Singleton {
    private Singleton() {}

    private static class SingletonHolder {
        public static final Singleton instance = new Singleton();
    }

    public static Singleton getInstance() {
        return SingletonHolder.instance;
    }

    public static void main(String[] args) {
        final Singleton s = getInstance();
        final Singleton s1 = getInstance();
        System.out.println(s);
        System.out.println(s1);
        System.out.println(s == s1);
    }
}

